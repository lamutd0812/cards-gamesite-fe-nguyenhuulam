import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import Home from './containers/Home/Home';
import { connect } from 'react-redux';

class App extends Component {

  render() {
    const routes = (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path='/marketplace' component={() => { 
          window.location.href = 'http://13.229.73.251:3000'; 
          return null;
        }}/>
        <Redirect to="/" />
      </Switch>
    );

    return (
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = {
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
