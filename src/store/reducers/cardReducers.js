import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../util/utility';

const initialState = {
  user1Cards: [],
  user2Cards: [],
  message: '',
  error: null,
  updateCardsSuccess: false,
};

const cardStart = (state) => {
  return updateObject(state, { error: null });
};

const fetchCardsError = (state, action) => {
  return updateObject(state, {
    error: action.error,
  });
};

const getUser1CardsSuccess = (state, action) => {
  return updateObject(state, {
    user1Cards: action.user1Cards,
    error: null
  });
};

const getUser2CardsSuccess = (state, action) => {
  return updateObject(state, {
    user2Cards: action.user2Cards,
    error: null
  });
};

const updateCardsSuccess = (state, action) => {
  return updateObject(state, {
    updateCardsSuccess: action.updateCardsSuccess,
    error: null
  });
};

const resetUpdateCardsSuccess = (state) => {
  return updateObject(state, {
    updateCardsSuccess: false,
    error: null
  });
};


const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CARDS_START: return cardStart(state);
    case actionTypes.CARDS_ERROR: return fetchCardsError(state, action);

    case actionTypes.SEE_USER1_CARDS_SUCCESS: return getUser1CardsSuccess(state, action);
    case actionTypes.SEE_USER2_CARDS_SUCCESS: return getUser2CardsSuccess(state, action);

    case actionTypes.UPDATE_CARDS_SUCCESS: return updateCardsSuccess(state, action);
    case actionTypes.RESET_UPDATE_CARDS_SUCCESS: return resetUpdateCardsSuccess(state);

    default: return state;
  }
}

export default cardReducer;