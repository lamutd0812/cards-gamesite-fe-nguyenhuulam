import axios from '../../util/axios';
import * as actionTypes from './actionTypes';

const cardStart = () => {
  return {
    type: actionTypes.CARDS_START
  };
};

const cardErrors = (error) => {
  return {
    type: actionTypes.CARDS_ERROR,
    error: error
  }
};

const getUserCardsSuccess = (data, userType) => {
  if (userType === 1) {
    return {
      type: actionTypes.SEE_USER1_CARDS_SUCCESS,
      user1Cards: data,
    }
  }
  if (userType === 2) {
    return {
      type: actionTypes.SEE_USER2_CARDS_SUCCESS,
      user2Cards: data,
    }
  }
};

const updateCardsSuccess = () => {
  return {
    type: actionTypes.UPDATE_CARDS_SUCCESS,
    updateCardsSuccess: true,
  }
}

export const resetUpdateCardsSuccess = () => (dispatch) => {
  dispatch({
    type: actionTypes.RESET_UPDATE_CARDS_SUCCESS
  })
};

export const seeCards = (email, userType) => (dispatch, getState) => {
  axios.get(`/cards/see-cards/${email}`)
  .then(res => {
    dispatch(getUserCardsSuccess(res.data.data, userType));
  })
  .catch(err => {
    if (err.response.data.statusCode === 404) {
      dispatch(getUserCardsSuccess([], userType));
    } else {
      dispatch(cardErrors(err.response.data.message));
    }
  });
}

export const updateCards = (email, password, cards) => (dispatch) => {
  const body = {
    email,
    password,
    cards,
  }
  dispatch(cardStart());
  axios.put('/cards/update-cards', body)
  .then(res => {
    if (res.data.statusCode === 200 && res.data.success) {
      dispatch(updateCardsSuccess());
    }
  })
  .catch(err => {
    dispatch(cardErrors(err.response.data.message));
  });
}
