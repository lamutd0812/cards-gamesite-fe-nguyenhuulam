import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://13.213.73.6:8080/'
});

export default instance;