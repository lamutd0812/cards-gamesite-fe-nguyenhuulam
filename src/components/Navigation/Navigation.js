import React, { Component } from 'react';
import { connect } from 'react-redux';

class Navigation extends Component {

    render() {
        return (
          <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
              <a className="navbar-brand" href="/">Cards game - gamesite</a>
            </nav>
          </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      // email: state.auth.email
    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);