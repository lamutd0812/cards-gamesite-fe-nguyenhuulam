import React, { Component } from 'react';
import CardsBattle from '../Cards/CardsBattle';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';

class Home extends Component {

    componentDidMount() {
      window.scrollTo(0, 0);
    }

    render() {
      return (
        <div>
          <CardsBattle />
          <ToastContainer autoClose={2000} />
          {this.props.error ? toast.error('Error: ' + this.props.error) : null}
        </div>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    error: state.card.error,
  }
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
