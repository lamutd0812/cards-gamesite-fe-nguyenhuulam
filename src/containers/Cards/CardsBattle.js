import React, { Component } from 'react';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import { connect } from 'react-redux';
import ConfirmDialog from '../../components/UI/ConfirmDialog/ConfirmDialog';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import { seeCards, updateCards, resetUpdateCardsSuccess } from '../../store/actions/cardActions';
import { Link } from 'react-router-dom';
import { updateObject } from '../../util/utility';

class CardsBattle extends Component {

  state = {
    email1: '',
    password1: '',
    email2: '',
    password2: '',
    user1Win: false,
    user2Win: false,
    user1MustUpdateCards: false,
    user2MustUpdateCards: false,
  }

  inputChangeHandler = (event) => {
    const controlName = event.target.name;
    switch (controlName) {
      case 'email1':
        this.setState({
          email1: event.target.value,
        });
        break;
      case 'email2':
        this.setState({
          email2: event.target.value,
        });
        break;
      case 'password1':
        this.setState({
          password1: event.target.value,
        });
        break;
        case 'password2':
          this.setState({
            password2: event.target.value,
          });
          break;
      default:
        break;
    }
  };

  seeUser1Cards = () => {
    this.props.seeCards(this.state.email1, 1);
  }

  seeUser2Cards = () => {
    this.props.seeCards(this.state.email2, 2);
  }

  selectWinner = () => {
    const min = 1;
    const max = 2;
    const random = Math.floor(Math.random() * (max - min + 1) + min);
    if (random === 1) {
      this.setState(updateObject(this.state, {
        user1Win: true,
        user1MustUpdateCards: false,
        user2Win: false, 
        user2MustUpdateCards: true,
      }));
    }
    if (random === 2) {
      this.setState(updateObject(this.state, {
        user2Win: true, 
        user2MustUpdateCards: false,
        user1Win: false, 
        user1MustUpdateCards: true,
      }));
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.updateCardsSuccess && !nextProps.error) {
      toast.success("Update card sussessfully!");
      this.props.resetUpdateCardsSuccess();
      this.props.seeCards(this.state.email1, 1);
      this.props.seeCards(this.state.email2, 2);
      this.setState({
        email1: '',
        password1: '',
        email2: '',
        password2: '',
        user1Win: false,
        user2Win: false,
        user1MustUpdateCards: false,
        user2MustUpdateCards: false,
      });
    }
  }

  getCardsData = (userCards) => {
    return userCards.map(item => {
      return {
        cardId: item.cardData._id,
        quantity: item.quantity,
      }
    })
  }

  confirmUpdateCardsHandler = () => {
    if (this.state.user1Win) {
      const user1Cards = this.getCardsData(this.props.user1Cards);
      this.props.updateCards(this.state.email2, this.state.password2, user1Cards);
    }
    if (this.state.user2Win) {
      const user2Cards = this.getCardsData(this.props.user2Cards);
      this.props.updateCards(this.state.email1, this.state.password1, user2Cards);
    }
  }

  render() {

    const userCardsList = (userCards, userType) => (
      <Aux>
        <div className="col-6 pl-1 p-2">
          <h6 className="font-weight-bold"> User {userType} 's cards: </h6>
          {userCards.length > 0 ? (
            userCards.map(card => (
              <Aux key={card._id}>
                {/* my card item */}
                <div className="card" style={{ width: '18rem' }}>
                  <div className="card-body">
                    <h5 className="card-title">Card name: {card.cardData.name}</h5>
                    <p className="card-text"> Power: {card.cardData.power}</p>
                    <p className="card-text"> Price: {card.cardData.price}</p>
                    <p className="card-text"> Quantity: {card.quantity}</p>
                  </div>
                </div>
              </Aux>
            ))
          ): (
            <div>
              This user has no cards, go to
              <Link to="/marketplace" target="_blank" rel="noopener noreferrer"> Marketplace </Link>
              to buy cards. 
            </div>
          )}
        </div>
      </Aux>
    );

    const user1BattleResult = (
      <Aux>
        {this.state.user1Win ? (
          <div className="col-6 pl-1 p-2">
            <h6 className="font-weight-bold"> User 1 is winner </h6>
          </div>
        ): null }
        {this.state.user1MustUpdateCards ? (
          <Aux>
            <div className="form-group ml-3">
              <div className="row">
                <div className="col-6 pl-1 p-2">
                  <h6 className="font-weight-bold"> User 1 is loser, enter password here to buy same cards: </h6>
                  <input
                    type="password"
                    name="password1"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>
              </div>
            </div>
            <div className="col-6 pl-1 p-2">
              <button
                type="submit"
                className="btn btn-dark"
                onClick={() => this.confirmUpdateCardsHandler()}
              >Update cards</button>
            </div>
          </Aux>
        ): null}
      </Aux>
    );

    const user2BattleResult = (
      <Aux>
        {this.state.user2Win ? (
          <div className="col-6 pl-1 p-2">
            <h6 className="font-weight-bold"> User 2 is winner </h6>
          </div>
        ): null }
        {this.state.user2MustUpdateCards ? (
          <Aux>
            <div className="form-group ml-3">
              <div className="row">
                <div className="col-6 pl-1 p-2">
                  <h6 className="font-weight-bold"> User 2 is loser, enter password here to buy same cards: </h6>
                  <input
                    type="password"
                    name="password2"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>
              </div>
            </div>
            <div className="col-6 pl-1 p-2">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={() => this.confirmUpdateCardsHandler()}
                  >Update cards</button>
                </div>
          </Aux>
        ): null}
      </Aux>
    );

    const contentWrapper = (
      <div className="form-group p-3">
        <h6 className="font-weight-bold ml-3"> Cards Battle </h6>
        <div className="form-group ml-3">
          <div className="row">
            <div className="col-sm border border-primary">
              <div className="col-6 pl-1 p-2">
                <label>User 1's email:</label>
                <input
                  type="text"
                  name="email1"
                  className="form-control"
                  onChange={this.inputChangeHandler} />
              </div>
              <div className="col-6 pl-1 p-2">
                <button
                  type="submit"
                  className="btn btn-dark"
                  onClick={() => this.seeUser1Cards()}
                >See cards</button>
              </div>
              {userCardsList(this.props.user1Cards, 1)}
              {user1BattleResult}
            </div>

            <div className="col-sm border border-primary">
              <div className="col-6 pl-1 p-2">
                <label>User 2's email:</label>
                <input
                  type="text"
                  name="email2"
                  className="form-control"
                  onChange={this.inputChangeHandler} />
              </div>
              <div className="col-6 pl-1 p-2">
                <button
                  type="submit"
                  className="btn btn-dark"
                  onClick={() => this.seeUser2Cards()}
                >See cards</button>
              </div>
              {userCardsList(this.props.user2Cards, 2)}
              {user2BattleResult}
            </div>
          </div>
        </div>

        <div className="form-group ml-3">
          { (!this.props.user1Cards.length || !this.props.user2Cards.length) 
          ? (
            <div className="col-6 pl-2 text-danger font-italic">
              Both users must have cards and different email to battle!
            </div>
          ) : null}
          <div className="row">
            <div className="col-6 pl-1 p-2">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={
                  (!this.props.user1Cards.length || !this.props.user2Cards.length)
                  || (this.state.email1 === this.state.email2)
                }
                onClick={this.selectWinner}
              >Battle</button>
            </div>
          </div>
        </div>
      </div>
    );

    return (
      <Aux>
        {contentWrapper}
        
        <ConfirmDialog
          title="Return card confirmation"
          message="Rerturn this card and get coins back?"
          confirm={this.confirmSubmitHandler} />
      </Aux>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user1Cards: state.card.user1Cards,
    user2Cards: state.card.user2Cards,
    updateCardsSuccess: state.card.updateCardsSuccess,
  }
}

const mapDispatchToProps = {
  seeCards,
  updateCards,
  resetUpdateCardsSuccess,
}

export default connect(mapStateToProps, mapDispatchToProps)(CardsBattle);